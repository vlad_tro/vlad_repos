#include "shape.h"

#ifndef H_POLYGON
#define H_POLYGON

class Polygon : public Shape {
public:
    Polygon();
	Polygon(int size_in);//+
	Polygon(vector <Point> points);//+
	Polygon(const Polygon& other);//+
	int getSize() ;//+

	bool cmp(Point a, Point b);

protected:
	vector <Point> points;

private:
    double area_triangle(Point a,Point b,Point c);
    double vc(Point a,Point b);
};

Polygon::Polygon(int size_in) {
    if (size_in < 3) {
		cout << "Error:it is not polygon" << endl;
		exit(0);
	}
	points.resize(size_in,Point(0,0));
}

bool Polygon::cmp(Point a, Point b) {
	return a.getX() < b.getX() || a.getX() == b.getX() && a.getY() < b.getY();
}

Polygon::Polygon(vector <Point> points_in) {
    if (points_in.size() < 3) {
		cout << "Error:it is not polygon" << endl;
		exit(0);
	}
    for (int i = 0; i < points_in.size(); ++i) {
		points.push_back(points_in[i]);
	}

	sort(points.begin(), points.end(), &cmp);
}

Polygon::Polygon(const Polygon& other) {
	for (int i = 0; i < other.points.size(); ++i) {
		points.push_back(other.points[i]);
	}
}

int Polygon::getSize() {
	return points.size();
}

double Polygon::vc(Point a,Point b)//[a,b]=|a||b|sinA
{
       return a.getX()*b.getY() -a.getY()*b.getX();
}

double Polygon::area_triangle (Point a,Point b,Point c)
{
      return (vc(a-b,a-c))/2;
}
double Polygon:: area()
{
     double s = 0;
     for (int i = 1;i <points.size()-1;++i)
     {
         s = s + area_triangle(points[i],points[i+1],points[0]);
     }
     return s;
}




bool Polygon::isPointOnBorder(Point& p) {
    Segment s;
    for (int i=0; points.size;i++) {
        s = Segment(points[i],points[(i+1)%(points.size())]);
        if (s.distPointSegment(p) == 0) return true;
    }
    return false;
}

enum { INSIDE, OUTSIDE, BOUNDARY };
//     ������, ���,     �� �������
enum { TOUCHING, CROSSING, INESSENTIAL };
//    �������b���, ������������, ��������������

int Polygon::isPointInShape(Point &a, Polygon &p)
{
  int parity = 0;
  int n = points.size();
  for (int i = 0;  i < n; i++) {
    Segment e = Segment(points[i],points[(i+1)%n]);
    switch (e.segmentType(a)) {
      case TOUCHING:
        return BOUNDARY;
      case CROSSING:
        parity = 1 - parity;
    }
  }
  return (parity ? INSIDE : OUTSIDE);
}

vector<Point> Polygon::intersectionWithSegment(Segment& s) {

    vector<Point> answer;
    Point a;
	Segment q;
    int n = points.size();
    for (int i = 0;  i < n; i++) {
		q = Segment(points[i], points[(i + 1) % n]);
		if (s.getA() / q.getA() != s.getB() / q.getB()) {
			a = s.lineLine(q);
			if (((a <= s.getSegB()) && (a >= s.getSegA())) || ((a >= s.getSegB()) && (a <= s.getSegA()))&& ((a <= q.getSegB()) && (a >= q.getSegA()))
				|| ((a >= q.getSegB()) && (a <= q.getSegA())))
				answer.push_back();
			
		}
    }
    return answer;
}
#endif

