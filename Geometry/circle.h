#include "shape.h"

#define _USE_MATH_DEFINES

const double PI = acos(-1);

class Circle : public Shape{
public:
	Circle() {};
	Circle(Point o, double r);
	Circle(Circle& c);

    double area();//+
	bool isPointOnBorder(Point& _A);
	bool isPointInShape(Point& _A);
	vector<Point> intersectionWithSegment(Segment& _AB);

	void setCenter(Point o);
	void setRadius(double r);

	Point getCenter();
	double getRadius();


private:
	Point o;
	double r;
};

Circle::Circle(Point o, double r) : o(o), r(r) {}

Circle::Circle(Circle& c) : o(c.getCenter()), r(c.getRadius()) { }


double Circle::getRadius() {
	return r;
}
Point Circle::getCenter() {
	return o;
}

double Circle::area() {
	return acos(-1)*r*r;
}

bool Circle::isPointOnBorder(Point& p){
    if ((o.getX()-p.getX())*(o.getX()-p.getX())+(o.getY()-p.getY())*(o.getY()-p.getY()) == r*r)
        return true;
    return false;
}


bool Circle::isPointInShape(Point& p) {
	if ((o.getX()-p.getX())*(o.getX()-p.getX())+(o.getY()-p.getY())*(o.getY()-p.getY()) <= r*r)
		return true;
	return false;
}

vector<Point> Circle::intersectionWithSegment(Segment& s){
    vector <Point> answer;
    double a,b,c,d4,x1,x2,y1,y2;
    a = s.getA()*s.getA() + s.getB()*s.getB();
    b = 2*s.getB()*s.getC() - 2*s.getB()*s.getA()*o.getX() - 2*(s.getA()*s.getA())*o.getY();
    c = s.getC()*s.getC() - 2*s.getC()*s.getA()*o.getX() + s.getA()*s.getA()*o.getY()*o.getY() + s.getA()*s.getA()*o.getX()*o.getX();
	d4 = (b / 2)*(b / 2) - a*c;
    y1 = (-(b/2) + sqrt(d4))/a;
    y2 = (-(b/2) - sqrt(d4))/a;
    x1 = -(s.getB()*y1 + s.getC())/s.getA();
    x2 = -(s.getB()*y2 + s.getC())/s.getA();

    Point p1 = Point(x1, y1);
    Point p2 = Point(x2, y2);

    if (s.distPointSegment(p1) == 0) answer.push_back(p1);
    if (s.distPointSegment(p2) == 0) answer.push_back(p2);
}


