#include <iostream>
#include <cmath>
#include <fstream>
#include <algorithm>
#include <vector>
#include <cstdlib>
#include <math.h>

using namespace std;

#ifndef H_POINT
#define H_POINT
class Point {
private:
    double x;
	double y;

public:
    Point(){ }
	Point(double x, double y);
	Point& operator+(Point& a);
	Point& operator-(Point& a);
	bool operator==(Point& p);
	bool operator<=(Point& p);
	bool operator>=(Point& p);

	double getX();
	double getY();

	double sc(Point a);//(a,b)=|a||b|cosA
	double vc(Point a);//[a,b]=|a||b|sinA

	double distPointPoint(Point a);
	double length();

	friend istream &operator >> (istream &cin, Point a);
	friend ostream &operator << (ostream &cout, Point &a);
};

double Point::getX() { return x;}
double Point::getY() { return y;}

double Point::distPointPoint(Point b)
{
       return sqrt((x-b.x)*(x-b.x)+(y-b.y)*(y-b.y));
}

double Point::length()
{
  return sqrt(x*x + y*y);
}

istream & operator >> (istream &cin, Point a)
{
   cin>>a.x>>a.y;
   return cin;
}
ostream & operator << (ostream &cout, Point &a)
{
    cout<<a.x<<" "<<a.y<<" ";
    return cout;
}

Point::Point(double x, double y) : x(x), y(y) {} 

Point& Point::operator+(Point& a) {
	x += a.x;
	y += a.y;
	return *this;
}

Point& Point::operator-(Point& a) {
	x -= a.x;
	y -= a.y;
	return *this;
}

bool Point::operator==(Point& p) {
	if (x == p.x && y == p.y) {
		return true;
	}
	return false;
}

bool Point::operator >= (Point& p) {
	if (x >= p.x && y >= p.y)  {
		return true;
	}
	return false;
}

bool Point::operator<=(Point& p) {
	if (*this >= p) {
		return false;
	}
	return true;
}
double Point::sc(Point a)//(a,b)=|a||b|cosA
{
	return a.getX()*x + a.getY()*y;
}
double Point::vc(Point a)//[a,b]=|a||b|sinA
{
	return a.getX()*y - a.getY()*x;
}
#endif
