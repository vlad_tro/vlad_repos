#include "rectangle.h"

class Square : public Rectangle {
public:
	Square(int size_in);
	Square(std::vector <Point> points);
	Square(const Square& other);
};

Square::Square(int size_in): Rectangle(size_in) { }

Square::Square(std::vector <Point> points) : Rectangle(points) {
	segment AB(pPoints[0], pPoints[1]);
	segment BC(pPoints[1], pPoints[2]);
	if (AB.length() != BC.length()) {
		std::cout << "Error" << std::endl;
		exit(6);
	}
}

Square::Square(const Square& other) : Rectangle(other) {}
