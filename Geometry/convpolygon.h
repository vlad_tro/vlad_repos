#include "polygon.h"

#ifndef H_CONVPOLYGON
#define H_CONVPOLYGON


class Convpolygon : public Polygon {
public:

	Convpolygon(int size_in = 0);
	Convpolygon(vector <Point> points);
	Convpolygon(const Convpolygon& other);


private:
	bool isConvex(Polygon &a);
};


Convpolygon::Convpolygon(int size_in): Polygon(size_in) {}

int sign(double a)
{
    if (a>0.00001)
       return 1;
    return -1;
}

bool Convpolygon:: isConvex (Polygon &a)
{
	int n;
     n = a.points.size();
     int z = sign(vc(a.points[0]-a.points[1],a.points[1]-a.points[2]));
     for (int i = 1; i < n; ++i)
     {
         if (z != sign(vc(a.points[i]-a.points[(i+1)%n],a.points[(i+1)%n]-a.points[(i+2)%n])))
            return false;
     }
     return true;
}

Convpolygon::Convpolygon(std::vector <Point> points_in): Polygon(points_in) {

	if (points.size() == 1) {
		return;
	}

	if (!isConvex())
        {
		std::cout << "Error: it is not Convex polygon" << std::endl;
		exit(0);
	}

}

Convpolygon::Convpolygon(const Convpolygon& other) {
	for (int i = 0; i < other.getSize(); ++i) {
		points[i].push_back(other.points[i]);
	}
}
#endif


