#include "rectangle.h"
#ifndef H_QUADRANGLE
#define H_QUADRANGLE

class Quadrangle : public Rectangle {
public:
	Quadrangle(int size_in = 0);
	Quadrangle(std::vector <Point> points_in);
	Quadrangle(Quadrangle& other);
};

Quadrangle::Quadrangle(int size_in ) : Rectangle(size_in) {}

Quadrangle::Quadrangle(std::vector <Point> points_in): Rectangle(points_in) {
	AC.setSeg(points[0], points[3]);
	AB.setSeg(points[0], points[1]);
	if (AB.length() != AC.length()) {
		std::cout << "Error:it is not quadrangle" << std::endl;
		exit(0);
	}
}

Quadrangle::Quadrangle(Quadrangle& other) : Rectangle(other) {}


#endif