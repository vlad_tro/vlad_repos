#include "point.h"
#ifndef H_SEGMENT
#define H_SEGMENT

class Segment  {
private:
    Point p1;
	Point p2;

public:
	Segment();
	Segment(Point& p1, Point& p2);

	double getA();
	double getB();
	double getC();


	double vc(Point a,Point b);
	double vc(Segment CD);
	double sc(Point a, Point b);//(a,b)=|a||b|cosA
	double sc(Segment CD);
	double length();
	int signVC(Point c);
	int signVC(Segment CD);
	Point vectorPoint();

    Segment perpendicular (Point b);
    Point segmentSegment (Segment q);
    Point lineLine(Segment q);
    double distPointSegment(Point a);
    int segmentType (Point &a);
	int position(Point p);

	void setSeg(Point, Point);


	Point getSegA();
	Point getSegB();

    istream &operator >> (istream &cin);
	ostream &operator << (ostream &cout);

	enum { LEFT, RIGHT, BEYOND, BEHIND, BETWEEN, ORIGIN, DESTINATION };
	//    �����, ������, �������, ������, �����,   ������, �����
};
istream & Segment::operator >> (istream &cin)
{
   cin >> p1 >> p2;
   return cin;
}
ostream & Segment::operator << (ostream &cout)
{
    cout<<p1<<" "<<p2<<" ";
    return cout;
}

Segment::Segment(Point& p1, Point& p2) : p1(p1), p2(p2) {}

Point Segment::getSegA() {
	return p1;
}

Point Segment::getSegB() {
	return p2;
}

double Segment::length()
{
	return ((p1.getX() - p2.getX()) * (p1.getX() - p2.getX()) +
		(p1.getY() - p2.getY()) * (p1.getY() - p2.getY()));
}

Point Segment::vectorPoint()
{

	double x = p2.getX() - p1.getX();
	double y = p2.getY() - p1.getY();
	Point c(x, y);
	return c;
}



double Segment::vc(Segment CD)
{
	Point a(vectorPoint());
	Point b(CD.vectorPoint());
	return b.vc(a);
}



double Segment::sc(Segment CD)
{
	Point a(vectorPoint());
	Point b(CD.vectorPoint());
	return b.sc(a);
}

int Segment::signVC(Point c) {
    Segment AC(p1, c);
	return signVC(AC);
}

int Segment::signVC(Segment CD)
{
	int k = vc(CD);
	if (k > 0) { return 1; }
	if (k < 0) { return -1; }
	return 0;
}

Segment Segment::perpendicular(Point p)
{
	double ap, bp, cp;
	ap = -getB();
    bp = getA();
	cp = -(ap*p.getX() + bp*p.getY());
     return Segment(Point((-bp-cp)/ap ,1), Point((-bp*2 - cp) / ap, 2));
}

double Segment::getA() { return p2.getY() - p1.getY(); }
double Segment::getB() { return p1.getX() - p2.getX(); }
double Segment::getC() { return p1.getX()*(p1.getY() - p2.getY()) + p1.getY()*(p2.getX() - p1.getX()); }

Point Segment::lineLine(Segment q)
{
	Point p;
	double a, b, c;
	double x, y;
    a = getA();
    b = getB();
    c = getC();


    y = ((q.getC())*a - c*q.getA())/(b*q.getA() - q.getB()*a);
    if (q.getA() != 0)
        x = (-q.getB()*p.getY() - q.getC())/q.getA() ;
    else
        x = (-b*p.getY() - c)/a;
    return Point(x,y);
}//��������� �������!

Point Segment::segmentSegment(Segment q) {
    Point p = lineLine(q);
    if (distPointSegment(p) == 0 && q.distPointSegment(p) == 0 )
        return p;
}

double Segment::distPointSegment(Point a)
{
   Segment p;
   p = perpendicular(a);
   Point t = lineLine(p);
   if ( ((t.getY() <= max(p2.getY(),p1.getY())) && (t.getY()) >= min(p2.getY(),p1.getY()))
       && ((t.getX() <= max(p2.getX(),p1.getX())) && (t.getX() >= min(p2.getX(),p1.getX()))) )
        return t.distPointPoint(a);
   else
       return min(a.distPointPoint(p1),a.distPointPoint(p2));
}



int Segment::position(Point p)
{
  Point a = p2 - p1;
  Point b = p - p1;
  double vc = b.vc(a);
  if (vc > 0.0)
    return LEFT;
  if (vc < 0.0)
    return RIGHT;
  if ((a.getX() * b.getX() < 0.0) || (a.getY() * b.getY() < 0.0))
    return BEHIND;
  if (a.length() < b.length())
    return BEYOND;
  if (p1 == p)
    return ORIGIN;
  if (p2 == p)
    return DESTINATION;
  return BETWEEN;
}

enum { TOUCHING, CROSSING, INESSENTIAL };
//     �������b���, ������������, ��������������

int Segment::segmentType (Point &a)
{
  switch (position(a)) {
    case LEFT:
      return ((p1.getY() < a.getY())&&(a.getY() <= p2.getY())) ? CROSSING : INESSENTIAL;
    case RIGHT:
      return ((p2.getY() < a.getY())&&(a.getY() <= p1.getY())) ? CROSSING : INESSENTIAL;
    case BETWEEN:
    case ORIGIN:
    case DESTINATION:
      return TOUCHING;
    default:
      return INESSENTIAL;
  }





#endif