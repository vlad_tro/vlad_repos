#include "segment.h"
#include "point.h"
#ifndef H_SHAPE
#define H_SHAPE

class Shape
{
public:
	virtual double area();
	virtual bool isPointOnBorder(Point& p);
	virtual bool isPointInShape(Point& p);
	virtual vector<Point> intersectionWithSegment(Segment& s);

};


#endif

