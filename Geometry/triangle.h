#include "polygon.h"

#ifndef H_TRIANGLE
 #define H_TRIANGLE

class Triangle : public Polygon {
public:
	Triangle(int size_in);
	Triangle(std::vector <Point> points_in);
	Triangle(const Triangle& other);

private:
    std::vector <Point> points;
	int size_p;
};


Triangle::Triangle(int size_in) : Polygon(size_in) {
	if (size_in() != 3) {
		std::cout << "Error:it is not triangle" << std::endl;
		exit(0);
	}
}

Triangle::Triangle(std::vector <Point> points_in) : Polygon(points_in) {
	if (points_in.size() != 3) {
		std::cout << "Error:it is not triangle" << std::endl;
		exit(0);
	}
}

Triangle::Triangle(const Triangle& other): Polygon(other) {}
#endif
