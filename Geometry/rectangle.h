#include "convpolygon.h"


#ifndef H_RECTANGLE
#define H_RECTANGLE

class Rectangle : public Convpolygon {
public:
	Rectangle(int size_in = 0);
	Rectangle(std::vector <Point> points);
	Rectangle(Rectangle& other);
};

Rectangle::Rectangle(int size_in ) : Convpolygon(size_in) {
    if (size_in != 4) {
		std::cout << "Error:it is not rectangle" << std::endl;
		exit(0);
	}
}

Rectangle::Rectangle(std::vector <Point> points) : Convpolygon(points) {
    segment AB(points[0], points[1]);
	segment CD(points[2], points[3]);
	if (AB.length() != CD.length()) {
		std::cout << "Error:it is not rectangle" << std::endl;
		exit(0);
	}
	if (AB.vc(CD) != 0) {
		std::cout << "Error:it is not rectangle" << std::endl;
		exit(0);
	}
	AC.setSeg(points[0], points[3]);
	AB.setSeg(points[0], points[1]);
	if (AB.sc(AC) != 0) {
		std::cout << "Error:it is not rectangle" << std::endl;
		exit(0);
	}
}

Rectangle::Rectangle(Rectangle& other) : Convpolygon(other) {}
#endif