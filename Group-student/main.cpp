#include <iostream>
#include <set>
#include <map>
#include <vector>
#include <string>	
#include <type_traits>
#include <Windows.h>

using std::vector;
using std::map;
using std::set;
using std::cout;
using std::cin;
using std::string;
using std::pair;
using std::make_pair;



class Student {
public:
	Student();
	/*Student(string exam, int mark) {
		info.insert(pair<string, int>( exam, mark));
	};*/
	Student(string st_name, string st_sirname, map<string, int> in) {
		info = in;
		name = st_name;
		sirname = st_sirname;
	};
	string name;
	string sirname;
	int get_number_of_ce() const {
		int number = 0;
		for (map<string, int> ::const_iterator iter = info.begin(); iter != info.end(); ++iter) 
			if ((*iter).second > 2) number++;
		return number;
	};
	int get_mark(string exam) const {
		return (info.find(exam))->second;
	};
private:
	map<string, int> info;  
};
struct setcomp {
	bool operator() (const Student & a, const Student & b) {
		return (a.sirname < b.sirname);
	}
};


class Group	 {
public:
	Group(set<Student, setcomp> stud_list) {
		St_base = stud_list;
	};
	set<Student, setcomp> St_base;

	vector<string> retake_list(string exam) {
		vector <string> list;
		for (set<Student, setcomp>::iterator it = St_base.begin(); it != St_base.end(); ++it) {
			int mark = it->get_mark(exam);
			if ( (mark == 0) || (mark == 1) || (mark == 2)) {
				list.push_back((*it).sirname);
			}
		}
		return list;
	};

	void exp_list(int k) {
		
		for (set<Student, setcomp>::iterator it = St_base.begin(); it != St_base.end(); ++it)
			if (it->get_number_of_ce() < k) {
				St_base.erase(it);
			};
	};

};




int main() {
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);
	int n;
	string st_name;
	string st_sirname;
	string exam;
	int mark;
	int noe;
	string subject;
	int k;
	set<Student, setcomp> students_list;
	map<string, int> st_exams;
	cout << "����� ���������� ��������� ";
	cin >>  n;
	for (int i = 0; i < n; i++) {
		cout << "����� ��� � ������� ";
		cin >> st_name >> st_sirname;
		cout << "����� ��������� ��������� ";
		cin >> noe;
		cout << "����� ������� � ������ �� ���� ";
		for (int j = 0; j < noe; j++) {
			cin >> exam >> mark;
			st_exams.insert(pair<string, int>(exam, mark));
		}
		Student person(st_name, st_sirname, st_exams);
		students_list.insert(person);
		st_exams.clear();
	};
	Group gr(students_list);
	cout << "����� ������� ";
	cin >> subject;
	vector<string> a = gr.retake_list(subject);
	for (vector<string>::iterator it = a.begin(); it != a.end(); it++) {
		cout << (*it) << ' ';
	}
	
	cout << '\n' << "����� ����� ��������� ";
	cin >> k;
	gr.exp_list(k);
	for (set<Student, setcomp>::iterator it = students_list.begin(); it != students_list.end(); ++it) {
		cout << (*it).name << ' '<< (*it).sirname << '\n';
	}

	//system("pause");
	return 0;
}