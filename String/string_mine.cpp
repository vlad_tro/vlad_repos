#include "string_mine.h"
String_mine::String_mine()
{
	len=0;
	capacity=16;
	buf=new char[capacity];
	buf[len]='\0';
}
String_mine::String_mine(const string& str):capacity(str.capacity()),len(str.length())
{
    buf = new char[capacity];
    strcpy(buf,str.data());
    buf[len] = '\0';
}
String_mine::String_mine(const char* str)
{
    if (str != NULL && strlen(str) != 0)
    {
        len=strlen(str);
        capacity=2*strlen(str);
        buf = new char[capacity];;
        strcpy(buf,str);
        buf[len]='\0';
    }
    else{
        len = 0;
        capacity = 16;
        buf = new char[capacity];
        buf[len] ='\0';
    }
}
String_mine::String_mine(const String_mine &str)
{
    if (str != 0) {
        len=str.len;
        capacity=str.capacity;
        buf = new char[capacity];
        strcpy(buf,str.buf);
        buf[len]='\0';
    }
}
String_mine& String_mine::operator =(const String_mine &src)
{
	if (this==&src)
		return (*this);
	len=src.len;
	capacity=src.capacity;
	if(buf != NULL)
        delete buf;
	buf = new char[capacity];
	strcpy(buf,src.buf);
	buf[len]='\0';
	return *this;
}
String_mine::~String_mine()
{
	delete buf;
}
int String_mine::GetLength()
{
	return len;
}
int String_mine::GetCapacity()
{
	return capacity;
}
const char* String_mine::GetString()const
{
	return buf;
}
void String_mine::DeleteInside(int index)
{
	if (index<=len)
    {
        memmove(&buf[index],&buf[index+1],sizeof(char)*(len-index-1));
        len--;
        buf[len]='\0';
    }
}
void String_mine::PushInside(char* str,int index)
{
    if (index<=len)
    {
        String_mine S;
        int k = strlen(str);
        S.buf = new char[capacity*2];
        memcpy(&S.buf[0],&buf[0],index);
        memcpy(&S.buf[index],&str[0],k);
        memcpy(&S.buf[index+k],&buf[index],len-index);
        len+=k;
        capacity*=2;
        S.buf[len]='\0';
        strcpy(buf,S.buf);
    }
}
void String_mine::DeleteLast(int index)
{
    if (index<=len)
    {
        len-=index;
        buf[len]='\0';
    }

}
void String_mine::PushBack(char *str)
{
	len+=strlen(str);
	capacity*=2;
	String_mine S;
	S.capacity = capacity*2;
	S.buf = new char[S.capacity];
	strcpy(S.buf,buf);
	buf = new char[S.capacity];
	strcat(S.buf,str);
	strcpy(buf,S.buf);
}
char& String_mine::operator [](int x)
{
	if (x>=0)
		return (buf[x]);
	else
		{
            cout << "Error\n";
		    return (buf[len+x]);
		}

}
bool operator ==(const String_mine& str1,const String_mine& str2)
{
	return strcmp(str1.buf,str2.buf)==0;
}
bool operator !=(const String_mine& str1,const String_mine& str2)
{
	return !(str1==str2);
}
bool operator >(const String_mine& str1,const String_mine& str2)
{
	if (strcmp(str2.buf,str1.buf) > 0)
        return true;
    else
        return false;
}
bool operator <(const String_mine& str1,const String_mine& str2)
{
	if (strcmp(str1.buf,str2.buf) < 0)
        return true;
    else
        return false;
}
bool operator <=(const String_mine& str1,const String_mine& str2)
{
	return !(str2>str1);
}
bool operator >=(const String_mine& str1,const String_mine& str2)
{
	return !(str1<str2);
}
String_mine String_mine::operator+(const String_mine& s)
{
	String_mine str1(*this);
	String_mine str2 = s;
	String_mine str = String_mine();
	if (str1.capacity>=str2.capacity)
		str.capacity = 2*str1.capacity;
	else
		str.capacity = 2*str2.capacity;
	str.len = str1.len + str2.len;
	str.buf = new char[str.capacity];
	strcpy(str.buf,str1.buf);
	strcat(str.buf,str2.buf);
	return str;
}
ostream& operator<<(ostream& ostr,const String_mine& str1)
{
	ostr<< str1.buf;
	return (ostr);
}
istream& operator>>(istream& is, String_mine &str)
{

	char c;
	if (str.len!=0)
	{
		delete [] str.buf;
		str.buf =(char*)malloc(sizeof(char)*10+1);
		str.len = 0;
		str.capacity = 10;
	}
	for(;;)
	{
	is.get(c);
	{
		if (c!=' ' && c!= '\n')
			{
			if (str.len==str.capacity)
				{
				str.capacity *= 2;
				str.buf=(char*)realloc(str.buf, sizeof(char)*str.capacity+1);
				}
				str.buf[str.len]	=c;
				++str.len;
		}
		else
			break;
	}
	}
	str.buf[str.len]='\0';
	return is;
}

