#include <iostream>
#include <vector>
#include <stack>
#include <string>



using std::vector;
using std::string;
using std::cin;
using std::cout;
using std::stack;


class Graph
{

private:
	bool label;
	vector<int> colour;
	vector<int> counters;
	vector< vector<int> > adjacency_list;
	vector<int> answer;
	int get_next_top(int work_top);


public:

	Graph();
	Graph(int number_vertices, const vector< vector<int> > & matrix);

	bool const is_visit(int top);
	void DFS(int top);
	void const get_answer();
};

Graph::Graph(int number_vertices, const vector< vector<int> > & matrix) :
	colour(number_vertices, 0),
	counters(number_vertices, 0),
	adjacency_list(matrix),
	label(true)
{}

int Graph::get_next_top(int work_top)
{
	//work_top - ������ ������� 
	//counters[work_top] - ���������� ���������� ������������� ��������� ������ �� ������
	return adjacency_list[work_top][counters[work_top]];
}

bool const Graph::is_visit(int top)
{
	return colour[top];
}

void Graph::DFS(int top)
{
	stack<int> work_top;
	work_top.push(top);
	while (work_top.size() > 0 && label)
	{
		if (!colour[work_top.top()])
			colour[work_top.top()] = 1;
		if (adjacency_list[work_top.top()].size() == counters[work_top.top()])
		{
			colour[work_top.top()] = 2;
			answer.push_back(work_top.top());
			work_top.pop();
		}
		else
		{
			if (colour[get_next_top(work_top.top())] == 1)
				label = false;
			else
				if (!colour[get_next_top(work_top.top())])
				{
					int toptop = work_top.top();
					work_top.push(get_next_top(work_top.top()));
					counters[toptop]++;
				}
				else
					counters[work_top.top()]++;
		}
	}

}

void const Graph::get_answer()
{
	cout << '\n';
	if (label)
	{
		cout << "Yes\n";
		for (int i = 0; i < answer.size(); i++)
			cout << answer[i] << ' ';
	}
	else cout << "No\n";
}


int main()
{
	int n, m;
	string tops_in;
	cin >> n;
	getline(cin, tops_in);
	vector< vector<int> > adl(n + 1, vector<int>(0));
	
	
	int top;
	for (int i = 1; i <= n; i++) {
		getline(cin,tops_in);
		//cout << tops_in.length() << '\n';
		for (int j = 0; j < tops_in.length(); j++) {
			if (tops_in[j] != ' ') {
				top = atoi(&tops_in[j]);
				if ((top == 0)||(top==i)) {
					cout << '0';
					return 0;
				}
				//cout << top << ':' << i << '\n';
				adl[i].push_back(top);
			}
		}
	}


	

	/*cout << n << '\n';
	for (int i = 0; i < n; i++) {
		cout << i << ':'; 
		for (int j = 0; j < adl[i].size(); j++) {
			cout << adl[i][j] << ' ';
		}
		cout << '\n';
	}*/

	Graph graph(n + 1, adl);

	for (int i = 1; i <= n; i++)
		if (!graph.is_visit(i))
			graph.DFS(i);

	graph.get_answer();
	//system("pause");
	return 0;
}
