#include <iostream>
#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cstdlib>

using namespace std;
class String_mine
{
public:
	int len;
	char* buf;
	int capacity;

	String_mine();
	String_mine(const string& s);
	String_mine(const char* str);
	String_mine(const String_mine& str);
	String_mine& operator=(const String_mine& src);
	~String_mine();
	int GetLength();
	int GetCapacity();
	const char* GetString()const;
	void DeleteInside(int index);
	void PushInside(char* str,int index);
	void DeleteLast(int index);
	void PushLast(char* str);
	void PushBack(char* s);
	char& operator[](int x);
	friend bool operator ==(const String_mine& str1,const String_mine& str2);
	friend bool operator !=(const String_mine& str1,const String_mine& str2);
	friend bool operator >(const String_mine& str1,const String_mine& str2);
	friend bool operator <(const String_mine& str1,const String_mine& str2);
	friend bool operator <=(const String_mine& str1,const String_mine& str2);
	friend bool operator >=(const String_mine& str1,const String_mine& str2);
	String_mine operator+(const String_mine& s);
	friend ostream& operator<< (ostream& ostr,const String_mine& str);
	friend istream& operator>> (istream& istr,String_mine& str1);
};
