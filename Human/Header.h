#include "string_mine.h"

class Human: public String_mine {
public:
	Human() {};
	Human(String_mine f, String_mine s);
	virtual void getTitle() const {};
protected:
	String_mine firstName, secondName;
};

Human::Human(String_mine f, String_mine s) : firstName(f) , secondName(s) {}

class Mister: public Human {
public:
	Mister() {};
	Mister(String_mine f, String_mine s) : Human(f, s) {};
	void getTitle() {
		cout << "Mr." << secondName << '\n';
	}
};

class Miss: public Human {
public:
	Miss() {};
	Miss(String_mine f, String_mine s) : Human(f, s) {};
	void getTitle() {
		cout << "Ms." << secondName << '\n';
	}
};

class Kid:public Human {
public:
	Kid() {};
	Kid(String_mine f, String_mine s) : Human(f, s) {};
	void getTitle() {
		cout << firstName << '\n';
	}
};