#include <iostream>
#include <vector>
#include <queue>
#include <string>



using std::vector;
using std::string;
using std::cin;
using std::cout;
using std::queue;

class Graph
{

private:
	vector<bool> used;
	vector<int> distance, parents;
	vector< vector<int> > adjacency_list;

public:

	Graph();
	Graph(int number_vertices);
	Graph(int number_vertices, const vector< vector<int> > & matrix);

	void BFS(int top);
	void path(int end);
	void read_adl();
};

Graph::Graph(int number_vertices, const vector< vector<int> > & matrix) :
	adjacency_list(matrix),
	distance(number_vertices, 0),
	parents(number_vertices, 0),
	used(number_vertices, false)
{}
Graph::Graph(int number_vertices) :
	adjacency_list(number_vertices, vector<int>(0)),
	distance(number_vertices, 0),
	parents(number_vertices, 0),
	used(number_vertices, false)
{}
void Graph::read_adl()
{
	int top;
	string tops_in;

	int n = adjacency_list.size();

	getline(cin, tops_in);
	for (int i = 1; i < n; i++)
	{
		getline(cin, tops_in);
		for (int j = 0; j < tops_in.length(); j++)
		{
			if (tops_in[j] != ' ')
			{
				top = atoi(&tops_in[j]);
				adjacency_list[i].push_back(top);
			}
		}
	}
}

void Graph::BFS(int top)
{
	queue<int> work_top;
	work_top.push(top);

	used[top] = true;
	parents[top] = -1;
	while (!work_top.empty()) {
		int v = work_top.front();
		work_top.pop();
		for (int i = 0; i < adjacency_list[v].size(); ++i) {
			int to = adjacency_list[v][i];
			if (!used[to]) {
				used[to] = true;
				work_top.push(to);
				distance[to] = distance[v] + 1;
				parents[to] = v;
			}
		}
	}
}

void Graph::path(int end)
{

	if (!used[end])
		cout << "Not enough adapters!";
	else {
		vector<int> answer;
		for (int v = end; v != -1; v = parents[v])
			answer.push_back(v);
		reverse(answer.begin(), answer.end());
		cout << answer.size() - 1 << '\n';
		for (int i = 0; i < answer.size() - 1; ++i)
			cout << answer[i] << " " << answer[i + 1] << '\n';
	}
}

int main() {

	int n, start, end;

	cin >> n;
	cin >> start >> end;

	Graph power_socket(n + 1);
	power_socket.read_adl();
	power_socket.BFS(start);
	power_socket.path(end);


	system("pause");
	return 0;
}

