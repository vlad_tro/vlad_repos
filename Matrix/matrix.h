#pragma once

#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <vector>
#include <iostream>
#include <sstream>
#include <string.h>
#include <math.h>
#include <time.h>
#include <cmath>
#include <string>
#include <algorithm>
#include <cstdlib>

using namespace std;
template< class T >
class Matrix
{
public:
	int h, w;
	T **p;

	void allocArrays()
	{
		p = new T*[h];
		for (int i = 0; i < h; i++)
			p[i] = new T[w];
	}

	Matrix();
	Matrix(int Height, int Width);
	Matrix(const Matrix &t);
	~Matrix();
	void _rand();
	Matrix< T > &operator+=(const Matrix<T> &t);
	Matrix< T > operator+(const Matrix<T> &t);
	Matrix< T > operator-(const Matrix<T> &t);
	Matrix< T > operator*(const Matrix<T> &t);

	T det() const;
	T track();
	Matrix< T > minor(int, int);
	Matrix< T > tran();
	Matrix< T > inversion();
	void _cin();
	void _cout();
	T* operator[](int);


};


#pragma once
