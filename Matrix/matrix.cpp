
#include "matrix.h"


#define abs(x) ((x) >= 0 ? (x) : (-x))

template <class T>
Matrix<T>::Matrix()
{
	h = 0;
	w = 0;
	allocArrays();
}
template <class T>
Matrix<T>::Matrix(int Height, int Width)
{
	h = Height;
	w = Width;
	allocArrays();
	for (int i = 0; i < h; i++)
		for (int j = 0; j < w; j++)
			p[i][j] = static_cast<T>(0);
}
template <class T>
Matrix<T>::Matrix(const Matrix<T> &t)
{
	h = t.h;
	w = t.w;
	allocArrays();
	for (int i = 0; i < h; i++)
		for (int j = 0; j < w; j++)
			p[i][j] = t.p[i][j];
}
template <class T>
Matrix<T>::~Matrix()
{
	for (int i = 0; i < h; i++)
		delete[] p[i];
	delete[] p;
}
template <class T>
void Matrix<T>::_rand()
{
	srand(time(NULL));
	for (int i = 0; i < h; i++)
		for (int j = 0; j < w; j++)
		{
			p[i][j] = (rand() % 10);
			p[i][j] = (T)p[i][j];
		}
}
template< class T >
Matrix< T > &Matrix< T >::operator+=(const Matrix< T > &t)
{
	for (int i = 0; i < h; ++i)
		for (int j = 0; j < w; ++j)
			p[i][j] += t.p[i][j];
	return *this;
}
template<class T>
Matrix< T > Matrix< T >::operator+(const Matrix< T > &t)
{
	Matrix< T >  temp(*this);
	temp._cout();
	cout << "\n";
	if ((h != t.h) || (w != t.w))
	{
		cout << "Error";
		return t;
	}
	else
	{
		return(temp += t);
	}
}
template< class T >
Matrix< T > Matrix< T >::operator-(const Matrix< T > &t)
{
	Matrix< T > t1 = *this;
	Matrix< T > t2 = t;
	Matrix< T >  temp = Matrix(t1.h, t1.w);
	if (t1.h != t2.h || t1.w != t2.w)
	{
		cout << "Error";
		return t;
	}
	else
	{

		for (int i = 0; i < t1.h; i++)
			for (int j = 0; j < t1.w; j++)
				temp.p[i][j] = t1.p[i][j] - t2.p[i][j];
		return temp;
	}
}
template< class T>
Matrix< T > Matrix< T >::operator*(const Matrix< T > &t)
{
	Matrix t1 = *this;
	Matrix t2 = t;

	if (t1.w != t2.h)
	{
		cout << "Impossible to calculate this operation";
		return t1;
	}
	else
	{
		Matrix M = Matrix(t1.h, t2.w);
		for (int i = 0; i < M.h; i++)
			for (int j = 0; j < M.w; j++)
				for (int k = 0; k < t1.w; k++)
					M.p[i][j] += t1.p[i][k] * t2.p[k][j];
		return M;
	}
}
template<class T>
T Matrix<T>::det() const
{
	Matrix< T > M1(*this);
	int height = M1.h;
	int width = M1.w;
	double** arr = new double*[h];
	for (int i = 0; i < h; i++)
		arr[i] = new double[w];
	for (int i = 0; i < height; i++)
		for (int j = 0; j < width; j++)
			arr[i][j] = double(M1.p[i][j]);


	const double EPS = 1E-9;
	double d = 1.0;
	for (int i = 0; i<height; ++i)
	{
		int k = i;
		for (int j = i + 1; j<height; ++j)
			if (abs(arr[j][i]) > abs(arr[k][i]))
				k = j;
		if (abs(arr[k][i]) < EPS)
		{
			d = 0.0;
			break;
		}
		swap(arr[i], arr[k]);
		if (i != k)
			d = -d;
		d *= arr[i][i];
		for (int j = i + 1; j<height; ++j)
			arr[i][j] = (arr[i][j]) / (arr[i][i]);
		for (int j = 0; j<height; ++j)
			if ((j != i) && (abs(arr[j][i]) > EPS))
				for (int k = i + 1; k<height; ++k)
				{
					arr[j][k] -= arr[i][k] * arr[j][i];
				}
	}
	for (int i = 0; i < height; i++)
		delete[] arr[i];
	delete[] arr;
	return (static_cast<T>(d));
}
template<class T>
T Matrix< T >::track()
{
	T tr = static_cast<T>(0);
	for (int i = 0; i < h; ++i)
		tr += p[i][i];

	return tr;
}
template<class T>
Matrix <T> Matrix< T >::tran()
{
	for (int i = 0; i < h; i++)
		for (int j = i; j < h; j++)
			swap(p[i][j], p[j][i]);
	return *this;
}
template < class T >
Matrix< T > Matrix< T >::minor(int str, int col)
{
	int i;
	int j;
	int k = 0;
	int l = 0;
	Matrix< T > minor(h - 1, w - 1);

	for (i = 0; i < h; i++)
		if (i != str)
		{
			for (j = 0; j < w; j++)
				if (j != col)
				{
					minor[k][l] = p[i][j];
					l++;
				}
			k++;
			l = 0;
		}
	return minor;
}

template<class T>
Matrix< T > Matrix< T >::inversion()
{
	Matrix< double > M1(h, w);
	int i = 0;
	int j = 0;

	if (h != w)
		cout << "Incorrect size.\n";
	if (det() == 0)
		cout << "Determinant = 0.\n";
	if (h == 1)
		M1[i][j] = pow(-1.0, i + j) / det();
	else
		for (i = 0; i < h; i++)
			for (j = 0; j < w; j++)
				M1[j][i] = pow(-1.0, i + j) * minor(i, j).det() / det();
	return M1;

}

template< class T >
void Matrix< T >::_cin()
{
	for (int i = 0; i < h; i++)
		for (int j = 0; j < w; i++)
			cin >> p[i][j];
}
template< class T >
void Matrix< T >::_cout()
{
	for (int i = 0; i < h; i++)
	{
		for (int j = 0; j < w; j++)
			cout << p[i][j] << " ";
		cout << endl;
	}
}

template < class T >
T* Matrix< T >::operator[](int i)
{
	return p[i];
}
