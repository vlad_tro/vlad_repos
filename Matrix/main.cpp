#include "matrix.h"
#include "matrix.cpp"
using namespace std;
int main()
{
	int x, y;
	cin >> x >> y;
	Matrix < int > MM(x, y);
	MM._rand();
	MM._cout(); cout << "\n";
	Matrix < int > NN(x, y);
	NN._rand();
	NN._cout(); cout << "\n";
	Matrix < int > Z = MM + NN;
	Z._cout(); cout << "\n";
	Z = MM - NN;
	Z._cout(); cout << "\n";
	Z = MM * NN;
	Z._cout(); cout << "\n";


	cin >> x >> y;
	Matrix < double > M(x, y);
	M._rand();
	M._cout();
	cout << "\n";


	if (x == y)
	{
		double tr = M.track();
		cout << "track = " << tr << endl;
		Matrix <double> K(M);
		double det = K.det();
		cout << "det(M) = " << det << endl;

		Matrix <double> N(M);
		N.tran();
		N._cout();

		Matrix <double> L(M);
		Matrix <double> B = L.inversion();
		B._cout();
	}
	else
		cout << "This matrix is not square";


	return 0;
}
