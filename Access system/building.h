#include "human.h"
#include <vector>

class Building {
public:
    Building() {};
    Building(std::string name_in);
    virtual bool permission(Human* p); 
	virtual std::string getName() { return name; }

private:
    std::string name;
};

Building::Building(std::string name_in) : name(name_in) { }

bool Building::permission(Human* people_in){
	std::cout << (*people_in).getName() << std::endl;
    Student* student = dynamic_cast<Student*>(people_in);

    if (student)
        return true;
    else
    {
        Employee* employee = dynamic_cast<Employee*>(people_in);
        if (employee)
            return true;
        else
            return false;
    }
}

class LearningCampus: public Building {
public:
    LearningCampus(std::string name_in);

};
LearningCampus::LearningCampus(std::string name_in) : Building(name_in) { }

class Lab: public Building
{
public:
	
    Lab(std::string name_in) : Building(name_in) { }
	Lab(std::string name_in, std::vector<Student> s) : Building(name_in), stud_list(s) { }
    bool permission(Human* p);
	void addStudent(Student* s);
	int size_list() { return stud_list.size();  }
	~Lab() { stud_list.clear(); }
private:
    std::vector<Student> stud_list;
};
void Lab::addStudent(Student* s) {
	stud_list.push_back(*s);
}

bool Lab::permission(Human* people_in){
	//std::cout << "lllllll" << std::endl;
	Student* student = dynamic_cast<Student*>(people_in);
	if (student) {
		//std::cout << (*student).getName() << std::endl;
		std::vector<Student>::iterator it = stud_list.begin();
		while (it != stud_list.end()) {
			if (*it == *student) {
				//std::cout << (*it).getName() << std::endl;
				return true;
			}
			it++;
		}
		return false;
	}
    

    Employee* employee = dynamic_cast<Employee*>(people_in);

    if (employee)
    {
        return true;
    }
	else return false;


}
