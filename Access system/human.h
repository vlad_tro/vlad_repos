#include <string>

class Human
{
public:
    Human();
    Human(std::string name_in);
    virtual std::string getName();
    virtual bool operator==(Human& h);
    virtual bool operator!=(Human& h);
protected:
	std::string name;
};

Human::Human(std::string name_in) : name(name_in) {}

std::string Human::getName() { return name;}

bool Human::operator==(Human& h) {
    if (name == h.name) return true;
    else return false;
}
bool Human::operator!=(Human& h) {
    if (name != h.name) return true;
    else return false;
}

class Student: public Human {
public:
	Student(std::string name_in) : Human(name_in) {}

};

class Employee: public Human {
public:
	Employee(std::string name_in) : Human(name_in) {}
};
